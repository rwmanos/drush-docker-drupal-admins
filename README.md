# drush-docker-drupal-admins

Docker image containing all the neccesary to allow administrators of a site to run drush commands on their Drupal 8 website

## Sync drupal code base from production

```
rsync -avz --include '/sites/sites.php' --exclude '/sites/*' --exclude '.git' /mnt/data/drupal/8.X.Y/ ./drupal/
```

## Image build and execution

Build the image with
```
docker build -t drush .
```
then get the image id and run it in interactive mode
```
docker images
docker run -i --privileged \
-e DRUSH_SITENAME='test-edu-d8-ldap.web.cern.ch' \
-e DRUSH_USERNAME='ds990108' \
-e DRUSH_PASSWORD='xxxxxxxxxxxxxxxxxxxxxxxx' \
-e DRUSH_HOST='dbod-drup8t1.cern.ch' \
-e DRUSH_PORT='5500' \
-e DRUSH_DATABASE='ds990108' \
-e DRUSH_EGROUP='drupal-admins-test-edu-d8-ldap' \
-e DRUSH_OWNER='eduardoa' \
-e DRUSH_DRUPAL_VERSION='8.5.6' \
-t <image_id> /bin/bash
```
or with a file
```
docker run -i --privileged --env-file ../env.list -t <IMAGE> /bin/bash
```


