#!/bin/sh

#This script will be the first thing executed by users to finalize the preparation of the environment
set -x

#Cleanup previous executions

umount /mnt/site
rm -rf /mnt/site
rm -rf /drupal/${DRUPAL_VERSION}/sites/${DRUSH_SITENAME}/


#mount the webdav site's folder
mkdir -p /mnt/site
mount -t davfs https://${DRUSH_SITENAME}/_webdav /mnt/site

#prepare the drush alias
envsubst '$DRUSH_SITENAME$DRUSH_HOST$DRUSH_EGROUP$DRUSH_OWNER$DRUSH_EGROUP$DRUSH_USERNAME$DRUSH_PASSWORD$DRUSH_DATABASE$DRUSH_PORT$DRUSH_DRUPAL_VERSION$DRUPAL_VERSION' < /drush/site.alias.drushrc.php > /drush/aliases/${DRUSH_SITENAME}.alias.drushrc.php

# create folder for the site
mkdir -p /drupal/${DRUPAL_VERSION}/sites/${DRUSH_SITENAME}/

#symlink all webdav files to the final destination
ln -s /mnt/site/* /drupal/${DRUPAL_VERSION}/sites/${DRUSH_SITENAME}/

#copy settings.php to the final destination
envsubst '$DRUSH_SITENAME$DRUSH_HOST$DRUSH_EGROUP$DRUSH_OWNER$DRUSH_EGROUP$DRUSH_USERNAME$DRUSH_PASSWORD$DRUSH_DATABASE$DRUSH_PORT$DRUSH_DRUPAL_VERSION$DRUPAL_VERSION' < /drush/settings.php > /drupal/${DRUPAL_VERSION}/sites/${DRUSH_SITENAME}/settings.php


cd /drupal/${DRUPAL_VERSION}/sites/${DRUSH_SITENAME}/

#enable php71
#source scl_source enable rh-php71
scl enable rh-php71 bash 
