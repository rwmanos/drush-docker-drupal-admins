FROM cern/cc7-base
MAINTAINER drupal-admins@cern.ch

# Set the Drush version.
ENV DRUSH_VERSION 8.1.17
ENV DRUPAL_VERSION 8.5.6

RUN yum clean all \
    && yum install -y centos-release-scl \
    && yum-config-manager --enable rhel-server-rhscl-7-rpms \
    && yum install -y rh-php71 rh-php71-php-pdo rh-php71-php-pdo_mysql \
    && yum install -y composer unzip which davfs2 gettext mysql

RUN scl enable rh-php71 bash

COPY docker-sleep /bin/docker-sleep
COPY prepare-enviroment.sh /usr/bin/prepare-enviroment.sh

RUN mkdir /drupal \
    && chmod 755 /drupal \
    && mkdir -p /drush/aliases \
    && chmod -R 755 /drush \
    && chmod 744 /usr/bin/prepare-enviroment.sh

COPY drush/drushrc.php /drush/drushrc.php
COPY drush/site.alias.drushrc.php /drush/site.alias.drushrc.php
COPY drush/settings.php /drush/settings.php

COPY drupal/ /drupal/$DRUPAL_VERSION/

# Install Drush 8 with the phar file.
RUN curl -fsSL -o /usr/local/bin/drush "https://github.com/drush-ops/drush/releases/download/$DRUSH_VERSION/drush.phar" && \
  chmod +x /usr/local/bin/drush && \
  mkdir /etc/drush && cp /drush/drushrc.php /etc/drush/


CMD ["/bin/docker-sleep"]

