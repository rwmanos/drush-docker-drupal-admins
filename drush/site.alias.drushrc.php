<?php

$aliases["$DRUSH_SITENAME"] = array(
        'uri' => "$DRUSH_SITENAME",
        'root' => '/drupal/$DRUPAL_VERSION/',
        'site-name' => "$DRUSH_SITENAME",
        'site-mail' => "$DRUSH_EGROUP@cern.ch",
        'sites-subdir' => "$DRUSH_SITENAME",
        'account-name' => "$DRUSH_OWNER",
        'account-mail' => "$DRUSH_EGROUP@cern.ch",
        //'account-pass' => 'ae435755c58fe1b7177e1bafc3fcf658',
        'vhost-ip'  => 'drupal8lb01.cern.ch',
        'vhost-short' => "$DRUSH_SITENAME",
        'vhost-uid' => "$DRUSH_USERNAME",
        'vhost-gid' => 'apache',
        'site-type' => 'official',
        'databases' => array (
                'default' => array (
                        'default' => array (
                                'driver' => 'mysqli',
                                'username' => "$DRUSH_USERNAME",
                                'password' => "$DRUSH_PASSWORD",
                                'host' => "$DRUSH_HOST",
                                'port' => "$DRUSH_PORT",
                                'database' => "$DRUSH_DATABASE",
                                ),
                        ),
                ),
         'admin-egroup' => "$DRUSH_EGROUP",
         'drupal-version' => "$DRUPAL_VERSION",
);

// drush needs db-url for site-install ...
$aliases['$DRUSH_SITENAME']['db-url']=
        $aliases['$DRUSH_SITENAME']['databases']['default']['default']['driver']."://".
        $aliases['$DRUSH_SITENAME']['databases']['default']['default']['username'].":".
        $aliases['$DRUSH_SITENAME']['databases']['default']['default']['password']."@".
        $aliases['$DRUSH_SITENAME']['databases']['default']['default']['host'].":".
        $aliases['$DRUSH_SITENAME']['databases']['default']['default']['port']."/".
        $aliases['$DRUSH_SITENAME']['databases']['default']['default']['database'];

?>
