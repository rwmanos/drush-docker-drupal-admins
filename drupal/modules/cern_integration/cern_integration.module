<?php

/* Debugging purposes */
//function cern_integration_form_alter(&$form, \Drupal\Core\Form\FormStateInterface $form_state, $form_id) {
//   drupal_set_message(t("Form ID: $form_id"), "status");
//}

//function cern_integration_form_menu_edit_item_alter(&$form, \Drupal\Core\Form\FormStateInterface $form_state, $form_id) {
//   drupal_set_message(t("Form ID: $form_id"), "status");
//}
/* Debugging purposes */

/**
* Implements hook_form_FORM_ID_alter() for system_modules_uninstall().
* Checks whether memcache module exists. If so, disable the possibility of uninstalling it. It's mandatory to have it installed.
*/
function cern_integration_form_system_modules_uninstall_alter(&$form, &$form_state, $form_id){
    $moduleHandler = \Drupal::service('module_handler');
    if ($moduleHandler->moduleExists('memcache')){
        $form['uninstall']['memcache']['#disabled'] = TRUE;

        global $base_url;
        $memcache_setup_link = \Drupal\Core\Link::fromTextAndUrl('Memcache Setup', \Drupal\Core\Url::fromUri("$base_url/_site/memcache.php"))->toString();

        $txt = '</br>';
        $txt .= '<em>Please, do not uninstall memcache module since it can break down your site.</em>';
        $txt .= '</br>';
        $txt .= "<em>If you do not want to use it, please go to " . $memcache_setup_link . ".</em>";

        $form['modules']['memcache']['description']['memcache_warning'] = array(
            '#markup' => t($txt)
        );
    }
}

/**
 * Implements hook_update_projects_alter().
 * Remove Drupal core as well as all centrally managed modules and themes from the Update manager.
 */
function cern_integration_update_projects_alter(&$projects)
{
    // Remove Drupal core.
    // unset($projects['drupal']);

    // Remove all centrally managed modules.
    foreach (system_get_info("module") as $module_name => $module) {
        if (preg_match('#^modules/#', drupal_get_path("module", $module_name))) {
            if (isset($module["project"])){
                unset($projects[$module["project"]]);
            }
        }
    }

    // Remove all centrally managed themes.
    foreach (system_get_info("theme") as $theme_name => $theme) {
        if (preg_match('#^themes/#', drupal_get_path("theme", $theme_name))) {
            if (isset($theme["project"])){
                unset($projects[$theme["project"]]);
            }
        }
    }
}

/**
* Implements hook_form_FORM_ID_alter() for views_exposed_form().
*/
//function cern_integration_form_views_exposed_form_alter(&$form, &$form_state, $form_id){
    //dpm($form);
    //drupal_set_message(t("VIEWS Exposed Form ID: $form_id"), "status");
//}

#################
# People        #
# /admin/people #
#################

/**
* Implements hook_form_FORM_ID_alter() for views_form_user_admin_people_page_1().
* Remove actions and headers related to user settings.
*/
function cern_integration_form_views_form_user_admin_people_page_1_alter(&$form, &$form_state, $form_id){
    $form['actions']['#disabled'] = TRUE;
    $form['header']['#disabled'] = TRUE;

    $txt = '<b>Actions concerning user accounts/roles are <em>DISABLED:</em></b><br/>All accounts are centrally managed at CERN via the portal at <a href="http://cern.ch/account">http://cern.ch/account</a> for accounts and passwords, and user roles through e-groups at <a href="http://e-groups.cern.ch">http://e-groups.cern.ch</a>';
    $form['header']['#prefix'] = t($txt);
}

/**
*  Implements hook_form_FORM_ID_alter() for user_register_form().
*  Disable fields under admin/people/create
*/
function cern_integration_form_user_register_form_alter(&$form, &$form_state, $form_id){
    drupal_set_message(t("Adding/Removing users is DISABLED."),"warning");
    // Disable all elements from this form.
    foreach (\Drupal\Core\Render\Element::children($form) as $key) {
        $form[$key]['#disabled'] = TRUE;
    }
}

/**
* Implements hook_module_implements_alter().
* @see https://cgit.drupalcode.org/simplesamlphp_auth/tree/simplesamlphp_auth.module
* @see cern_integration_form_user_form_alter(&$form, &$form_state, $form_id)
* As this module injects a checkbox on user_form, cern_integration module must be unstacked.
*/
function cern_integration_module_implements_alter(&$implementations, $hook) {
  if ($hook == 'form_alter' && isset($implementations['cern_integration'])) {
    // Move my_module_form_alter() to the end of the list.
    // \Drupal::moduleHandler()->getImplementations()
    // iterates through $implementations with a foreach loop which PHP iterates
    // in the order that the items were added, so to move an item to the end of
    // the array, we remove it and then add it.
    $group = $implementations['cern_integration'];
    unset($implementations['cern_integration']);
    $implementations['cern_integration'] = $group;
  }
}

/*
*  Implements hook_form_FORM_ID_alter() for user_form().
*  Disable fields under admin/people -> Edit username
*  @see /user/<id>/edit?destination=/admin/people
*/
function cern_integration_form_user_form_alter(&$form, &$form_state, $form_id){
    $form['account']['mail']['#disabled'] = TRUE;
    $form['account']['name']['#disabled'] = TRUE;
    $form['account']['pass']['#disabled'] = TRUE;
    $form['account']['current_pass']['#disabled'] = TRUE;
    $form['account']['status']['#disabled'] = TRUE;
    $form['account']['roles']['#disabled'] = TRUE;

    // This comes from cern_integration_module_implements_alter, executing the module to the last.
    $form['simplesamlphp_auth_user_enable']['#disabled'] = TRUE;

    unset($form['actions']['delete']);
}

################################
# Account settings             #
# admin/config/people/accounts #
################################
/**
*  Implements hook_form_FORM_ID_alter() for user_admin_settings().
* @see /admin/config/people/accounts
*/
function cern_integration_form_user_admin_settings_alter(&$form, &$form_state, $form_id){
    $form['registration_cancellation']['#disabled'] = TRUE;
    $txt = '<b>Changing registration and cancellation settings is <em>DISABLED:</em></b><br/>All accounts are centrally managed at CERN via the portal at <a href="http://cern.ch/account">http://cern.ch/account</a> for accounts and passwords, and user roles through e-groups at <a href="http://e-groups.cern.ch">http://e-groups.cern.ch</a>';
    $form['registration_cancellation']['#prefix'] = t($txt);
}

###########################################
# SimpleSAMLphp Auth                      #
# /admin/config/people/simplesamlphp_auth #
###########################################
/**
*  Implements hook_form_FORM_ID_alter() for simplesamlphp_auth_basic_settings_form().
* @see /admin/config/people/simplesamlphp_auth
*/
function cern_integration_form_simplesamlphp_auth_basic_settings_form_alter(&$form, &$form_state, $form_id){
    $form['basic']['activate']['#disabled'] = TRUE;
    $form['basic']['auth_source']['#disabled'] = TRUE;
    $form['basic']['login_link_display_name']['#disabled'] = TRUE;

    $form['user_provisioning']['register_users']['#disabled'] = TRUE;
}

/**
*  Implements hook_form_FORM_ID_alter() simplesamlphp_auth_local_settings_form().
* @see /admin/config/people/simplesamlphp_auth/local
*/
function cern_integration_form_simplesamlphp_auth_local_settings_form_alter(&$form, &$form_state, $form_id){
    $form['authentication']['allow_default_login']['#disabled'] = TRUE;
    $form['authentication']['allow_set_drupal_pwd']['#disabled'] = TRUE;
    $form['authentication']['allow_default_login_roles']['#disabled'] = TRUE;
    $form['authentication']['allow_default_login_users']['#disabled'] = TRUE;
}

/**
*  Implements hook_form_FORM_ID_alter() simplesamlphp_auth_syncing_settings_form().
* @see /admin/config/people/simplesamlphp_auth/sync
*/
function cern_integration_form_simplesamlphp_auth_syncing_settings_form_alter(&$form, &$form_state, $form_id){
    $form['user_info']['unique_id']['#disabled'] = TRUE;
    $form['user_info']['user_name']['#disabled'] = TRUE;
    $form['user_info']['user_name_sync']['#disabled'] = TRUE;
    $form['user_info']['mail_attr']['#disabled'] = TRUE;
    $form['user_info']['mail_attr_sync']['#disabled'] = TRUE;
    $form['user_info']['mail_attr_sync']['#disabled'] = TRUE;
    $form['user_info']['role_eval_every_time']['#disabled'] = TRUE;
    $form['user_info']['autoenablesaml']['#disabled'] = TRUE;
}

################################
# Matomo                        #
# /admin/config/system/matomo  #
################################
/**
*  Implements hook_form_FORM_ID_alter() for matomo_admin_settings().
* @see /admin/config/system/matomo
*/
function cern_integration_form_matomo_admin_settings_alter(&$form, &$form_state, $form_id){
    $form['general']['matomo_site_id']['#disabled'] = TRUE;
    $form['general']['matomo_url_http']['#disabled'] = TRUE;
    $form['general']['matomo_url_https']['#disabled'] = TRUE;

    global $base_url;
    $_finalurl = $base_url;
    $_searches = array("http://","https://",".web.cern.ch");
    $_finalurl = str_replace($_searches, "", $_finalurl);

    $form['cern_view_statistics'] = array(
        '#type' => 'fieldset',
        '#title' => t('View statistics'),
        '#weight' => - 30
    );

    $_markup = \Drupal\Core\Link::fromTextAndUrl('WebServices website', \Drupal\Core\Url::fromUri("https://webservices.web.cern.ch/webservices/Tools/Piwik/?SiteName=$_finalurl"))->toString();
    $form['cern_view_statistics']['matomo_link'] = array(
        '#markup' => "Please go to " . $_markup . " to view statistics gathered by Matomo/Piwik for your site."
    );
}

######################################
# Logging and errors                 #
# /admin/config/development/logging  #
######################################
/**
*  Implements hook_form_FORM_ID_alter() for system_logging_settings().
* @see /admin/config/development/logging
*/
function cern_integration_form_system_logging_settings_alter(&$form, &$form_state, $form_id){
    $form['syslog_identity']['#disabled'] = TRUE;
    $form['syslog_facility']['#disabled'] = TRUE;
    $form['syslog_format']['#disabled'] = TRUE;
}

######################################
# Help                               #
######################################
/**
* Implements hook_help().
*/
function cern_integration_help($route_name, \Drupal\Core\Routing\RouteMatchInterface $route_match) {
    switch($route_name){
        case 'help.page.cern_integration':
            $output = '';
            $output .= '<h3>' . t('About') . '</h3>';
            $output .= '<p>' . t('Custom functionality and constraints for CERN Drupal 8 infrastructure.') . '</p>';
      return $output;
    }
}
