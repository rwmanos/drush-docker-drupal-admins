(function ($, Drupal) {

    'use strict';
  
    jQuery(function(){
        var excludeForms = ':not(.view-general-search):not(.cern-view-display-resources):not(.cern-view-display-feature_events):not(.cern-view-display-past_events):not(.cern-view-display-all_news):not(.cern-view-display-faq_page):not(.cern-view-display-page_taxonomies)';
        exposedForms(excludeForms);
    });
    
    jQuery( window ).resize(function() {
        var excludeForms = ':not(.view-general-search):not(.cern-view-display-resources):not(.cern-view-display-feature_events):not(.cern-view-display-past_events):not(.cern-view-display-all_news):not(.cern-view-display-faq_page):not(.cern-view-display-page_taxonomies)';
        exposedForms(excludeForms);
    });

    function exposedForms(excludeForms) {
        var totalWidthForm = 0;
        /* jQuery('.cern-page-display-page .views-exposed-form .form-inline .form-item:not(:first-child), .cern-view-display-page'+excludeForms+' .views-exposed-form .form-inline .form-item:not(:first-child)').each(function(){
            var itemsCount = jQuery('.cern-page-display-page .views-exposed-form .form-inline .form-item:not(:first-child), .cern-view-display-page'+excludeForms+' .views-exposed-form .form-inline .form-item:not(:first-child)').length + 1;
            jQuery(this).css('width', (jQuery(this).parent().width()/itemsCount)-20 + 'px' );
            totalWidthForm += (jQuery(this).parent().width()/itemsCount)-20+20;
        });
        jQuery('.cern-page-display-page .views-exposed-form .form-inline .form-actions, .cern-view-display-page'+excludeForms+' .views-exposed-form .form-inline .form-actions').each(function(){
            var itemsCount = jQuery('.cern-page-display-page .views-exposed-form .form-inline .form-item:not(:first-child), .cern-view-display-page'+excludeForms+' .views-exposed-form .form-inline .form-item:not(:first-child)').length + 1;
            jQuery(this).css('width', (jQuery(this).parent().width()/itemsCount)-45 + 'px' );
            totalWidthForm += (jQuery(this).parent().width()/itemsCount)-45+20;
        }); */
        /* if (jQuery('.cern-view-display-page'+excludeForms+' .views-exposed-form .form-inline .form-item:first-child').width() > totalWidthForm) {
            jQuery('.cern-view-display-page'+excludeForms+' .views-exposed-form .form-inline .form-item:nth-child(2)')
            .css('margin-left', ((jQuery('.cern-view-display-page'+excludeForms+' .views-exposed-form .form-inline .form-item:first-child').width() - totalWidthForm+10)/2) + 'px')
        } */
    }

    jQuery(function(){
        carruselFillGaps();
    });
    
    jQuery( window ).resize(function() {
        clearFillGaps()
        carruselFillGaps();
    });

    function clearFillGaps() {
        jQuery(':not(.ux-library) .cern-view-display-block').each(function() {            
            jQuery(this).find('.owl-carousel .owl-item').each(function(){
                var itemWrapper = jQuery(this).find('.row');  
                if (itemWrapper.find('.views-row').length < 3) {  
                    itemWrapper.find('.views-row').each(function() {
                        jQuery(this).css('width', '');
                    });
                }
            });
        });
    }

    function carruselFillGaps() {
        if (jQuery(window).width() >= 992) {
            var idCarrusel = 0;
            jQuery(':not(.ux-library) .cern-view-display-block:not(.cern-view-display-story_resources):not(.cern-view-display-upcoming_events)').each(function() {
                idCarrusel++;
                jQuery(this).find('.owl-carousel .owl-item').each(function(){
                    var itemWrapper = jQuery(this).find('.row');    
        
                    if (itemWrapper.find('.views-row').length == 2) {
                        itemWrapper.find('.views-row').each(function(){
                            jQuery(this).css('width', 'calc(50% - 16px)');
                        });
                    }
        
                    if (itemWrapper.find('.views-row').length == 1) {
                        itemWrapper.find('.views-row').each(function(){
                            jQuery(this).css('width', 'calc(100% - 16px)');
                        });
                    } 
        
                });
            });
            /* jQuery(':not(.ux-library) .cern-view-display-block.cern-view-display-story_resources').each(function() {
                idCarrusel++;
                jQuery(this).find('.owl-carousel .owl-item').each(function(){
                    var itemWrapper = jQuery(this).find('.row');    
        
                    if (itemWrapper.find('.views-row').length == 5) {
                        itemWrapper.find('.views-row').each(function(){
                            jQuery(this).css('width', 'calc(50% - 16px)');
                        });
                    }
        
                    if (itemWrapper.find('.views-row').length == 4) {
                        itemWrapper.find('.views-row').each(function(){
                            jQuery(this).css('width', 'calc(50% - 16px)');
                        });
                    }
        
                    if (itemWrapper.find('.views-row').length == 3) {
                        itemWrapper.find('.views-row').each(function(){
                            jQuery(this).css('width', 'calc(50% - 16px)');
                        });
                    }
        
                    if (itemWrapper.find('.views-row').length == 2) {
                        itemWrapper.find('.views-row').each(function(){
                            jQuery(this).css('width', 'calc(50% - 16px)');
                        });
                    }
        
                    if (itemWrapper.find('.views-row').length == 1) {
                        itemWrapper.find('.views-row').each(function(){
                            jQuery(this).css('width', 'calc(100% - 16px)');
                        });
                    } 
        
                });
            }); */
        }
    }

})(jQuery, Drupal);
